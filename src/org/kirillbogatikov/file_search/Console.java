package org.kirillbogatikov.file_search;

import java.util.ArrayList;
import java.util.Scanner;

public class Console {
    static Scanner scanner = new Scanner(System.in);
    static SearchEngine engine; 
    
    public static void main(String[] args) {
        System.out.print("Enter path to root directory: ");
        String path = scanner.nextLine();
        
        System.out.print("Ok! Enter search text (1-256 symbols): ");
        String searchText = scanner.nextLine();
        
        engine = new SearchEngine(path, searchText);
        AnotherThread thread = new AnotherThread();
        
        try {
            ArrayList<SearchResult> results = engine.process();
            thread.start();
            System.out.printf("# -         PATH         - LINE\n");
            
            SearchResult result;
            for(int i = 0; i < results.size(); i++) {
                result = results.get(i);
                System.out.printf("%d - %s - %d\n", (i + 1), result.path, result.line + 1);
            }
        } catch (SearchException e) {
            e.printStackTrace();
        }
    }

    static class AnotherThread extends Thread implements Runnable {
        public void run() {
            long lastUpdate = 0;
            while(engine.isProcessing()) {
                if(System.currentTimeMillis() - lastUpdate >= 5000) {
                    lastUpdate = System.currentTimeMillis();
                    engine.printCurrentInfo();
                }
            }
        }
    }
}
