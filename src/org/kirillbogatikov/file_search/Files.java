package org.kirillbogatikov.file_search;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Files {
    static Scanner scanner;
    static SearchEngine engine; 
    
    public static void main(String[] args) {
        try {
            scanner = new Scanner(new FileInputStream("input.txt"));
            
            System.setOut(new PrintStream(new FileOutputStream("output.txt")));
            System.setErr(new PrintStream(new FileOutputStream("errors.txt")));
            
            String path = scanner.nextLine();
            String searchText = scanner.nextLine();
            
            engine = new SearchEngine(path, searchText);
            
            AnotherThread thread = new AnotherThread();
            ArrayList<SearchResult> results = engine.process();
            thread.start();
            
            System.out.printf("# -         PATH         - LINE\n");
            
            SearchResult result;
            for(int i = 0; i < results.size(); i++) {
                result = results.get(i);
                System.out.printf("%d - %s - %d\n", (i + 1), result.path, result.line + 1);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class AnotherThread extends Thread implements Runnable {
        public void run() {
            long lastUpdate = 0;
            while(engine.isProcessing()) {
                if(System.currentTimeMillis() - lastUpdate >= 5000) {
                    lastUpdate = System.currentTimeMillis();
                    engine.printCurrentInfo();
                }
            }
        }
    }
}
