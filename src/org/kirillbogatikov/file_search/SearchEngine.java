package org.kirillbogatikov.file_search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class SearchEngine {
    private String searchText;
    private int dirs, files;
    private boolean processing;
    private File work, current;
    private ArrayList<SearchResult> results;
    
    public SearchEngine(String workDir, String searchText) {
        this.work = new File(workDir);
        this.searchText = searchText;
        this.results = new ArrayList<>();
    }
    
    public boolean canProcess() {
        return this.work.exists() && this.work.isDirectory() && !searchText.isEmpty();
    }
    
    public ArrayList<SearchResult> process() throws SearchException {
        if(!this.work.exists() || !this.work.isDirectory()) {
            throw new SearchException("Root directory is not exists or it is not a directory");
        }
        
        if(searchText.isEmpty()) {
            throw new SearchException("Search text is empty");
        }
        
        results.clear();
        
        processing = true;
        searchInDir(this.work);
        processing = false;
        
        return results;
    }
    
    public boolean isProcessing() {
        return processing;
    }
    
    public void printCurrentInfo() {
        System.out.printf("Opened %d directories, readed %d files...\nText founded in %d files...\n", dirs, files, results.size());
        if(isProcessing())
            System.out.printf("Current file: %s\n", current.getAbsolutePath());
    }
    
    private boolean searchInDir(File dir) {
        File[] content = dir.listFiles();
        
        if(content == null) {
            return true;
        }
        
        dirs++;
        
        for(File f : content) {
            if(f.isDirectory() && searchInDir(f)) {
                return true;
            }
            
            if(f.isFile()) {
                searchInFile(f);
            }
        }
        
        return false;
    }
    
    private void searchInFile(File file) {
        this.current = file;
        files++;
        
        StringBuilder builder = new StringBuilder();
        
        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while((line = reader.readLine()) != null) {
                builder.append(line).append("\n");
            }
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
        
        String fileContent = builder.toString();
        int i = fileContent.indexOf(this.searchText);
        if(i != -1) {
            fileContent = fileContent.substring(0, i);
            int lineCount = fileContent.split("\n").length - 1;
            
            results.add(new SearchResult(file.getAbsolutePath(), lineCount));
        }
    }
}
