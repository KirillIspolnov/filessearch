package org.kirillbogatikov.file_search;

@SuppressWarnings("serial")
public class SearchException extends Exception {
    public SearchException(String m) {
        super(m);
    }
}
