package org.kirillbogatikov.file_search;

public class SearchResult {
    public String path;
    public int line;
    
    public SearchResult(String path, int l) {
        this.path = path;
        this.line = l;
    }
}
