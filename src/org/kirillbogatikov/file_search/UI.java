package org.kirillbogatikov.file_search;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class UI {
    static JFrame frame;
    static JPanel pane;
    static JButton ok;
    static JTextField path;
    static JTextField search;
    static JTextArea log;
    static SearchEngine engine;
    
    static Thread thread2, thread3;
    
    public static void main(String[] args) {
        thread2 = new SecondThread();
        thread3 = new ThirdThread();
        
        frame = new JFrame("UI Version of Searcher");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        pane = new JPanel();
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
        frame.setContentPane(pane);
        
        path = new JTextField();
        path.setText("PATH");
        frame.add(path);
        
        search = new JTextField();
        search.setText("SEARCH");
        frame.add(search);
        
        ok = new JButton("OK");
        ok.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                thread2.start();
                ok.setEnabled(false);
            }
        });
        frame.add(ok);
        
        log = new JTextArea();
        log.setEditable(false);
        frame.add(log);
        
        frame.pack();
        frame.setVisible(true);
    }

    static class SecondThread extends Thread implements Runnable {
        public void run() {
            engine = new SearchEngine(path.getText(), search.getText());
            thread3.start();
            
            long lastUpdate = 0;
            while(engine.isProcessing()) {
                if(System.currentTimeMillis() - lastUpdate >= 5000) {
                    lastUpdate = System.currentTimeMillis();
                    engine.printCurrentInfo();
                }
            }
        }
    }
    
    static class ThirdThread extends Thread implements Runnable {
        public void run() {
            try {
                ArrayList<SearchResult> results = engine.process();
                log.append("# -         PATH         - LINE\n");
                
                SearchResult result;
                for(int i = 0; i < results.size(); i++) {
                    result = results.get(i);
                    log.append(String.format("%d - %s - %d\n", (i + 1), result.path, result.line + 1));
                    frame.pack();
                }
            } catch (SearchException e) {
                e.printStackTrace();
            }
        }
    }
}
